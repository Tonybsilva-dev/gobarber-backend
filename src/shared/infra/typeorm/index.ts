import { createConnection } from 'typeorm';
import AppError from '../../errors/AppError';

const port = 5432

try {
  createConnection().then(
    () => console.log(`✅ Postgres connected on port ${process.env.TYPEORM_PORT || port}!`)
  ); //Importa o ormconfig.json
} catch (error) {
    throw new AppError('Postgres not connected.', 500);
}
