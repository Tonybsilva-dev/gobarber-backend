import cors from 'cors';
import express from 'express';
import 'express-async-errors';
import 'reflect-metadata';
import swaggerUi from "swagger-ui-express";
import UploadConfig from '../../../config/upload';
import swaggerFile from "../../docs/swagger.json";
import '../typeorm';
import GlobalError from './middlewares/GlobalError';
import { Logger } from './middlewares/Logger';
import { LogRequests } from './middlewares/LogRequests';
import routes from './routes';

const app = express();
app.use(cors())
app.use(express.json());
app.use(Logger);
app.use(LogRequests);
app.use('/files', express.static(UploadConfig.directory));
app.use('/api',routes);
app.use("/api-docs", swaggerUi.serve, swaggerUi.setup(swaggerFile));
app.use(GlobalError);


export { app };
