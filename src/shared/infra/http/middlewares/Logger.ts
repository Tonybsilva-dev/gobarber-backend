import expressPino from "express-pino-logger";

const Logger = expressPino({
  level: "info"
});

export { Logger }
