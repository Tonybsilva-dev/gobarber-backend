import { appointmentsRouter } from '@modules/appointments/routes/appointments.routes';
import { authRouter } from '@modules/auth/routes/auth.routes';
import { usersRouter } from '@modules/users/routes/users.routes';
import { Router } from 'express';

const routes = Router();

routes.get('/', (request, response) => {
  return response.json({
    application: 'GoBarber',
    status: 'Online'
  })
})

routes.use('/users', usersRouter);
routes.use('/appointments', appointmentsRouter);
routes.use('/auth', authRouter)

export default routes;

