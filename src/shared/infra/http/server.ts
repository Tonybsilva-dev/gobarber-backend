import { app } from "./app"

app.listen(process.env.PORT_SERVER || 3333, () => {
  console.log(`✅ Server connected on port ${process.env.PORT_SERVER}!`)
})
