import { EntityRepository, Repository } from 'typeorm'
import Appointment from '../infra/typeorm/entities/Appointment';

@EntityRepository(Appointment)
class AppointmentsRepository extends Repository<Appointment> {

  public async findByProviderID(provider_id: string): Promise<Appointment | null> {

    const findAppointment = await this.findOne({
      where: { provider_id },
    });

    return findAppointment || null;
  }

  public async findByDate(date: Date): Promise<Appointment | null> {

    const findAppointment = await this.findOne({
      where: { date },
    })
    return findAppointment || null;
  }
}

export default AppointmentsRepository;
