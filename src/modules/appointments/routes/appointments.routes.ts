import { Request, Response, Router } from 'express';
import { createAppointmentController } from '../useCases/CreateAppointment';

const appointmentsRouter = Router();

appointmentsRouter.post('/', (request: Request, response: Response) => {
  return createAppointmentController.store(request, response);
});

export { appointmentsRouter };
