import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  UpdateDateColumn,
  ManyToOne,
  JoinColumn } from 'typeorm';

import User from '@modules/users/infra/typeorm/entities/User'
import { uuid } from 'uuidv4';
import { BaseEntityDefault } from '@modules/base/infra/typeorm/BaseEntityDefault';

@Entity('appointments')
class Appointment extends BaseEntityDefault {

  @Column('uuid')
  provider_id: string;

  @ManyToOne(() => User)
  @JoinColumn({name: 'provider_id'})
  provider: User;

  @Column('timestamp with time zone')
  date: Date;

  @Column()
  status: string;

}

export default Appointment;
