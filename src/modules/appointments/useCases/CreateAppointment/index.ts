import { CreateAppointmentController } from "./CreateAppointmentController";
import { CreateAppointmentUseCase } from "./CreateAppointmentUseCase";


const createAppointmentUseCase = new CreateAppointmentUseCase();

const createAppointmentController = new CreateAppointmentController(createAppointmentUseCase);

export { createAppointmentUseCase, createAppointmentController };
