import { MissingParamError } from '@shared/errors';
import { Request, Response } from 'express';
import { CreateAppointmentUseCase } from './CreateAppointmentUseCase';
import { parseISO } from 'date-fns';


export class CreateAppointmentController {

  constructor(private createAppointmentUseCase: CreateAppointmentUseCase) { }

  async store(request: Request, response: Response) {

    const { provider_id, date } = request.body;

    const requiredField = ['provider_id', 'date']

    for (const field of requiredField) {
      if (!request.body[field]) {
         throw new MissingParamError(field)
      }
    }

    const parsedDate = parseISO(date);

    const createAppointment = new CreateAppointmentUseCase();

    const appointment = await createAppointment.execute({
      provider_id,
      date: parsedDate
    });

    return response.json(appointment)
  }
}
