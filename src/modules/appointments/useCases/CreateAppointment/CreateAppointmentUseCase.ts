import Appointment from "@modules/appointments/infra/typeorm/entities/Appointment";
import AppointmentsRepository from "@modules/appointments/repositories/AppointmentsRepository";
import { AppError } from "@shared/errors";
import { startOfHour } from "date-fns";
import { getCustomRepository } from "typeorm";
import { ICreateAppointmentDTO } from "./CreateAppointmentDTO";


export class CreateAppointmentUseCase {
  public async execute({ provider_id, date }: ICreateAppointmentDTO) : Promise<Appointment>{
    const appointmentsRepository = getCustomRepository(AppointmentsRepository);

    const appointmentDate = startOfHour(date);

    const findAppointmentInSameDate = await appointmentsRepository.findByDate(
      appointmentDate,
    );

    if (findAppointmentInSameDate) {
      throw new AppError('This appointment is already booked');
    }

    const appointment = appointmentsRepository.create({
      provider_id,
      date: appointmentDate,
      status: 'Approved'
    });

    await appointmentsRepository.save(appointment);

    return appointment;
  }
}
