export interface IUpdateAvatarUserDTO {
  user_id: string;
  avatarFileName: string;
}
