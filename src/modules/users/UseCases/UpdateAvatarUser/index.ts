import { UpdateAvatarUserController } from "./UpdateAvatarUserController";
import { UpdateAvatarUserUseCase } from "./UpdateAvatarUserUseCase";

const updateAvatarUserUseCase = new UpdateAvatarUserUseCase();

const updateAvatarUserController = new UpdateAvatarUserController(updateAvatarUserUseCase);

export { updateAvatarUserUseCase, updateAvatarUserController };
