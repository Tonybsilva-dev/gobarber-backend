import uploadConfig from '@config/upload';
import { AppError } from '@shared/errors/AppError';
import fs from 'fs';
import path from 'path';
import { getRepository } from 'typeorm';
import User from '../../infra/typeorm/entities/User';
import { IUpdateAvatarUserDTO } from './UpdateAvatarUserDTO';


export class UpdateAvatarUserUseCase {
  public async execute({ user_id, avatarFileName }: IUpdateAvatarUserDTO): Promise<User> {

    const usersRepository = getRepository(User)

    const user = await usersRepository.findOne({
      where: {
        id: user_id
      }
    })

    if (!user) {
      throw new AppError('User does not exists.', 404)
    }

    if (user.is_active !== true) {
      throw new AppError('User is inactive.', 401)
    }

    if (user.avatar) {
      const userAvatarFilePath = path.join(uploadConfig.directory, user.avatar);

      const userAvatarFileExists = await fs.promises.stat(userAvatarFilePath);

      console.log(userAvatarFileExists);

      if (userAvatarFileExists) {
        await fs.promises.unlink(userAvatarFilePath)
      }
    }

    user.avatar = avatarFileName;

    await usersRepository.save(user)

    return user;

  }
}
