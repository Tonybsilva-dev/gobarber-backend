import { Request, Response } from "express";
import { UpdateAvatarUserUseCase } from "./UpdateAvatarUserUseCase";


export class UpdateAvatarUserController {
  constructor(private updateAvatarUserUseCase: UpdateAvatarUserUseCase){}

  async store (request:Request, response: Response) {

    const updateAvatarUserUseCase = new UpdateAvatarUserUseCase();

    const user = updateAvatarUserUseCase.execute({
      user_id: request.user.id,
      avatarFileName: request.file.filename
    });

    delete (await user).password

    return response.json(user);

  }
}
