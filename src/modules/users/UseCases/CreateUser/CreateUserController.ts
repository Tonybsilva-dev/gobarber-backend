import { MissingParamError } from '@shared/errors';
import { Request, Response } from 'express';
import { CreateUserUseCase } from './CreateUserUseCase';

export class CreateUserController {

  constructor(private createUserUseCase: CreateUserUseCase) { }

  async store(request: Request, response: Response) {

    const { name, email, password } = request.body;

    const requiredField = ['name', 'email', 'password']

    for (const field of requiredField) {
      if (!request.body[field]) {
         throw new MissingParamError(field)
      }
    }

    const createUser = new CreateUserUseCase();

    const user = await createUser.execute({
      name,
      email,
      password,
    });

    delete user.password;

    return response.json(user)
  }
}
