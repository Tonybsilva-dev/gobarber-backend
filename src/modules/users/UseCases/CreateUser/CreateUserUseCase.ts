import { AppError } from '@shared/errors/AppError'
import { hash } from 'bcryptjs'
import { getRepository } from 'typeorm'
import User from '../../infra/typeorm/entities/User'
import { ICreateUserDTO } from './CreateUserDTO'

export class CreateUserUseCase {
  public async execute({ name, email, password }: ICreateUserDTO): Promise<User> {
    const usersRepository = getRepository(User)

    const checkUserExists = await usersRepository.findOne({
      where: { email },
    });

    if (checkUserExists) {
      throw new AppError('Email address already used.')
    }

    const hashedPassword = await hash(password, 8)

    const user = usersRepository.create({
      name,
      email,
      password: hashedPassword,
      is_active: true,
      type: 'user',
    });

    await usersRepository.save(user)

    return user;
  }
}
