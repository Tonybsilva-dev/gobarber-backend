import { BaseEntityDefault } from '@modules/base/infra/typeorm/BaseEntityDefault';
import {
  Column, CreateDateColumn, Entity, PrimaryGeneratedColumn, UpdateDateColumn
} from 'typeorm';

import { uuid } from 'uuidv4'


@Entity('users')
class User extends BaseEntityDefault {

  @Column()
  name: string;

  @Column()
  email: string;

  @Column()
  password: string;

  @Column()
  avatar: string;

  @Column()
  street: string;

  @Column()
  suite: string;

  @Column()
  city: string;

  @Column()
  zip_code: string;

  @Column()
  _doc: string;

  @Column()
  is_active: boolean;

  @Column()
  type: string;

}

export default User;
