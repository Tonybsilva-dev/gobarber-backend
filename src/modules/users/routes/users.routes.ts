import { Request, Response, Router } from 'express';
import { createUserController } from '../UseCases/CreateUser';
import multer from 'multer';
import UploadConfig from '@config/upload';
import { updateAvatarUserController } from '../UseCases/UpdateAvatarUser';
import { ensureAuthenticated } from '@shared/infra/http/middlewares/EnsureAuthenticated';

const usersRouter = Router();
const upload = multer(UploadConfig);


usersRouter.post('/', (request: Request, response: Response) => {
  return createUserController.store(request, response);
});

usersRouter.patch('/avatar', ensureAuthenticated, upload.single('avatar'), async (request: any, response) => {
  return updateAvatarUserController.store(request, response)
})

export { usersRouter };
