import { PrimaryColumn, CreateDateColumn, UpdateDateColumn } from "typeorm";
import { uuid } from "uuidv4";

class BaseEntityDefault {
  @PrimaryColumn()
  id: string;

  @CreateDateColumn()
  created_at: Date;

  @UpdateDateColumn()
  updated_at: Date;

  constructor() {
    if (!this.id) {
      this.id = uuid();
    }
  }
}

export { BaseEntityDefault }
