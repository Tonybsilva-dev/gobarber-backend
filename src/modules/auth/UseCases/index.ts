import { AuthUserController } from './AuthUserController';
import { AuthUserUseCase } from './AuthUserUseCase';

const authUserUseCase = new AuthUserUseCase();

const authUserController = new AuthUserController(authUserUseCase);

export { authUserUseCase, authUserController };
