# feat

### environment

- [x] Typescript
- [x] Docker
  - [x] Backend
  - [x] Database
  - [x] Redis
- [ ] Eslint
- [ ] Prettier
- [ ] EditorConfig
- [x] Logger
- [x] ErrorHandling

### user

- [x] Create a user
- [ ] Get a profile
- [ ] Update avatar
- [ ] Desactive account
- [ ] Update password
- [ ] Delete account

### appointments

- [x] Create an appointment
- [ ] Get an appointment in the same date

### status

- [ ] Pending - The approval step is not active and the approval step is not completed or canceled.
- [ ] Approved - The approval step is approved by an provider and the status moves to completed.
- [ ] Denied - The denied step is not approved by provider and status moves to finished.
- [ ] Canceled - The approval step is canceled by user.
- [ ] Finished - The finished is final step when the request is denied for provider.
- [ ] Completed - The completed is final step after the request is finished and aproved.
