require("dotenv").config();

module.exports = {
	type: "postgres",
	host: process.env.TYPEORM_HOST || "localhost",
	port: parseInt(process.env.TYPEORM_PORT, 10) || 5432,
	username: process.env.TYPEORM_USERNAME || 'postgres',
	password: process.env.TYPEORM_PASSWORD || '123546',
	database: process.env.TYPEORM_DATABASE || 'postgres',
  migrationsRun: true,
  "synchronize": false,
  "logging": true,
  "entities": [
    "./src/modules/**/infra/typeorm/entities/*.ts"
  ],
  "migrations": [
    "./src/shared/infra/typeorm/migrations/*.ts"
  ],
  "cli":{
    "migrationsDir": "./src/shared/infra/typeorm/migrations"
  }
}
